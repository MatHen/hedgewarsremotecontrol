# CMake generated Testfile for 
# Source directory: /usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone
# Build directory: /usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(hellfire_burns.lua "bin/hwengine" "--prefix" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/share/hedgewars/Data" "--nosound" "--nomusic" "--stats-only" "--lua-test" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/tests/lua/hellfire_burns.lua")
add_test(luaAPI/gravity_get_set.lua "bin/hwengine" "--prefix" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/share/hedgewars/Data" "--nosound" "--nomusic" "--stats-only" "--lua-test" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/tests/lua/luaAPI/gravity_get_set.lua")
add_test(luaAPI/zoom_get_set.lua "bin/hwengine" "--prefix" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/share/hedgewars/Data" "--nosound" "--nomusic" "--stats-only" "--lua-test" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/tests/lua/luaAPI/zoom_get_set.lua")
add_test(twothousandmines.lua "bin/hwengine" "--prefix" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/share/hedgewars/Data" "--nosound" "--nomusic" "--stats-only" "--lua-test" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/tests/lua/twothousandmines.lua")
add_test(drillrockets_boom.lua "bin/hwengine" "--prefix" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/share/hedgewars/Data" "--nosound" "--nomusic" "--stats-only" "--lua-test" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/tests/lua/drillrockets_boom.lua")
add_test(drillrockets_drill.lua "bin/hwengine" "--prefix" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/share/hedgewars/Data" "--nosound" "--nomusic" "--stats-only" "--lua-test" "/usr/home/unC0Rr/Sources/Hedgewars/Hedgewars-Clone/tests/lua/drillrockets_drill.lua")
subdirs(gameServer)
subdirs(misc/libphyslayer)
subdirs(hedgewars)
subdirs(bin)
subdirs(QTfrontend)
subdirs(share)
subdirs(tools)
