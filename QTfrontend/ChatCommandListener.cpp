/*
 * Hedgewars, a free turn based strategy game
 * Copyright (c) 2004-2015 Andrey Korotaev <unC0Rr@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * This file was created by Matthias Hennemann <hennemann.matthias@gmail.com> on
 * 17/07/2016
 *
 */


#include <ChatCommandListener.h>
#include <QTcpServer>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <game.h>
#include <hwform.h>
#include <proto.h>
#include <chrono>
#include <thread>

using namespace std;

ChatCommandListener::ChatCommandListener(HWForm* hwForm, QObject* parent) :
	QObject(parent)
{
	cout << "Listener Started" << endl;
	this->hwForm = hwForm;

	// Init command map
	commandMap["StartGame"] = Command::StartGame;
	commandMap["Forward"] = Command::Forward;

	// Start server
	startListening();
}

void ChatCommandListener::HandleConnection() {
	cout << "Game Controller Connected." << endl;

	// Get Socket
	socket = server->nextPendingConnection();

	// Connect socket to slots
	connect(socket, SIGNAL(disconnected()), this, SLOT(HandleDisconnect()));
	connect(socket, SIGNAL(readyRead()), this, SLOT(HandleMessage()));
}

/// <summary>
/// Registers the given HWGame object to this command listener.
/// </summary>
/// <param name="game">The game.</param>
void ChatCommandListener::registerGame(HWGame* game)
{
	cout << "Game registered." << endl;
	this->game = game;
}

void ChatCommandListener::parseCommand(string commandString)
{
	commandString.erase(commandString.length() - 2, 2);
	// Remove newline characters
	//commandString.erase(std::remove(commandString.begin(), commandString.end(), '\n'), commandString.end());

	cout << "Parsing: ," << commandString << ", beginning" << endl;

	// Take command elements
	size_t delimiterPos = commandString.find(' ');
	string command = commandString.substr(0, delimiterPos);
	string argument = commandString.erase(0, delimiterPos + 1);

	// Is this command in the command map?
	if (commandMap.find(command) != commandMap.end()) {

		// Execute command
		executeCommand(commandMap[command], argument);
	}
}

void ChatCommandListener::executeCommand(Command command, string args)
{
	cout << "Executing: " << command << " " << args << endl;
	switch (command)
	{
	case ChatCommandListener::Forward:
	{
		string s = args;
		game->SendIPC(QString(s.c_str()).toUtf8());
	}
	break;
	case ChatCommandListener::StartGame:
	{
		startGameRemotely();
	}
	break;
	default:
		return;
	}

}

void ChatCommandListener::startListening()
{
	// Wait for Connection
	server = new QTcpServer(0);
	server->setMaxPendingConnections(1);
	connect(server, SIGNAL(newConnection()), this, SLOT(HandleConnection()));
	bool success = server->listen(QHostAddress::Any, port);

	if (!success) {
		// Exit or throw error
		cout << "Error creating Server." << endl;
	}
}

void ChatCommandListener::startGameRemotely()
{
	cout << "Game started remotely" << endl;
	hwForm->StartGameForStream();
}

void ChatCommandListener::sendMessage(MessageType messageType, QString parameter)
{
	if (socket == NULL) return;
	string messageText;
	switch (messageType) {
	case MessageType::GameEnd:
		cout << "Sending GameEnd" << endl;
		messageText = "GameEnd";
		break;
	case MessageType::TurnStart:
		cout << "Sending TurnStart" << endl;
		socket->write((QString("TurnStart") + " " + parameter + "\n").toUtf8());
		break;
	case MessageType::TurnEnd:
		cout << "Sending TurnEnd" << endl;
		messageText = "TurnEnd";
		break;
	case MessageType::WeaponChange:
		cout << "Sending WeaponChange" << endl;
		socket->write((QString("WeaponChange") + " " + parameter + "\n").toUtf8());
		break;
	case MessageType::Theme:
		cout << "Sending Theme" << endl;
		socket->write((QString("Theme") + " " + parameter + "\n").toUtf8());
		break;
	case MessageType::GameResult:
		cout << "Sending GameResult" << endl;
		socket->write((QString("GameResult") + " " + parameter + "\n").toUtf8());
		break;
	case MessageType::MovePhaseEnded:
		cout << "Sending MovePhaseEnded" << endl;
		socket->write((QString("MovePhaseEnded")).toUtf8());
		break;
	case MessageType::TurnResult:
		cout << "Sending TurnResult " << string(parameter.toUtf8().constData()) << endl;
		socket->write((QString("TurnResult") + " " + parameter + "\n").toUtf8());
		break;
	case MessageType::HedgehogDied:
		cout << "Sending Hedgehog died " << string(parameter.toUtf8().constData()) << endl;
		socket->write((QString("HogDied") + " " + parameter + "\n").toUtf8());
		break;
	case MessageType::HogNames:
		cout << "Sending Hognames" << string(parameter.toUtf8().constData()) << endl;
		socket->write((QString("HogNames") + " " + parameter + "\n").toUtf8());
		break;
	case MessageType::AmmoUpdate:
		cout << "Sending AmmoUpdate" << string(parameter.toUtf8().constData()) << endl;
		socket->write((QString("AmmoUpdate") + " " + parameter + "\n").toUtf8());
		break;
	default:
		throw invalid_argument("Unhandled message type: " + messageType);
	}
	messageText = messageText + "\n";
	socket->write(QString(messageText.c_str()).toUtf8());
}

void ChatCommandListener::addTeam(HWTeam team)
{
	cout << "Team added: " + string(team.name().toUtf8().constData()) + " Color: " + to_string(team.color()) << endl;
	teams.push_back(team);
}

vector<string>& ChatCommandListener::split(const string &s, char delim, vector<string> &elems) {
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


vector<string> ChatCommandListener::split(const string &s, char delim) {
	vector<string> elems;
	split(s, delim, elems);
	return elems;
}

void ChatCommandListener::HandleDisconnect()
{
	cout << "Client Disconnected" << endl;

}

void ChatCommandListener::informWinningTeam(QString winningString) {
	if (winningString.contains("Green", Qt::CaseInsensitive)) {
		// Green won
		sendMessage(MessageType::GameResult, QString("Green"));
		return;
	}
	if (winningString.contains("Blue", Qt::CaseInsensitive)) {
		// Blue won
		sendMessage(MessageType::GameResult, QString("Blue"));
		return;
	}
	if (winningString.contains("draw", Qt::CaseInsensitive)) {
		// Draw
		sendMessage(MessageType::GameResult, QString("Draw"));
		return;
	}
}


void ChatCommandListener::addTurnResult(quint32 clan, quint32 hp) {
	QColor clanQColor(clan);
	for (int i = 0; i < teams.size(); i++) {
		if (teams[i].qcolor() == clanQColor) {
			// Guarantess that end turn message is only sent once
			if (i == 0) {
				// Indicate that turn ended
				sendMessage(MessageType::TurnEnd);
			}

			// TODO: Send turn results if required
			cout << "This is team " << string(teams[i].name().toUtf8().constData()) << endl;
			sendMessage(MessageType::TurnResult, teams[i].name() + " " + QString::number(hp));
		}
	}
}

void ChatCommandListener::informGameEnded()
{
	sendMessage(MessageType::GameEnd);
}

void ChatCommandListener::informTheme(QString themeName) {
	cout << "Thme set to " << themeName.toUtf8().constData() << endl;
	sendMessage(MessageType::Theme, themeName);
}

void ChatCommandListener::informHogDied(QString hogName)
{
	cout << "Received death info" << endl;
	sendMessage(MessageType::HedgehogDied, hogName);
}

void ChatCommandListener::informHogNames(QString namesGreen, QString namesBlue)
{
	sendMessage(MessageType::HogNames, namesGreen + " " + namesBlue);
}

void ChatCommandListener::informAmmoUpdate(QString updateString)
{
	sendMessage(MessageType::AmmoUpdate, updateString);
}

void ChatCommandListener::informWeaponChosen(QString weaponName)
{
	sendMessage(MessageType::WeaponChange, weaponName);
}

void ChatCommandListener::informTurnStarted(QString clanName)
{
	cout << "Clan " << clanName.toUtf8().constData() << " is now playing!" << endl;
	sendMessage(MessageType::TurnStart, clanName);
}

void ChatCommandListener::informMovePhaseEnded()
{
	cout << "Move phase ended" << endl;
	sendMessage(MessageType::MovePhaseEnded);
}

void ChatCommandListener::HandleMessage()
{
	while (socket->canReadLine()) {
		// Get message
		QByteArray read = socket->readLine();
		if (read.isEmpty()) return;

		// Parse and execute message
		parseCommand(read.data());
	}
}
