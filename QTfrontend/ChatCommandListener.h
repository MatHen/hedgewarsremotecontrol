/*
 * Hedgewars, a free turn based strategy game
 * Copyright (c) 2004-2015 Andrey Korotaev <unC0Rr@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * This file was created by Matthias Hennemann <hennemann.matthias@gmail.com> on
 * 17/07/2016
 *
 */

#pragma once

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QPointer>

class HWGame;
class HWForm;
class HWTeam;

class ChatCommandListener : public QObject
{
	Q_OBJECT

public:
	ChatCommandListener(HWForm* hwForm, QObject* parent = 0);
	void registerGame(HWGame* game);
	void addTeam(HWTeam team);
	void addTurnResult(quint32 clan, quint32 hp);
	void informGameEnded();
	void informTheme(QString themeName);
	void informWeaponChosen(QString weaponName);
	void informTurnStarted(QString clan);
	void informMovePhaseEnded();
	void informWinningTeam(QString winningString);
	void informHogDied(QString hogName);
	void informHogNames(QString namesGreen, QString namesBlue);
	void informAmmoUpdate(QString updateString);
private:
	enum Command { StartGame, Forward };
	enum MessageType { TurnStart, TurnEnd, GameEnd, WeaponChange, Theme, MovePhaseEnded, GameResult, TurnResult, HogNames, HedgehogDied, AmmoUpdate };
	const int port = 55556;
	HWGame* game;
	HWForm* hwForm;
	QPointer<QTcpServer> server;
	QPointer<QTcpSocket> socket;
	std::map<std::string, Command> commandMap;
	std::vector<HWTeam> teams;
	void parseCommand(std::string command);
	void executeCommand(Command command, std::string args);
	void startListening();
	void startGameRemotely();
	void sendMessage(MessageType messageType, QString parameter = "");
	std::vector<std::string>& split(const std::string &s, char delim, std::vector<std::string> &elems);
	std::vector<std::string> split(const std::string &s, char delim);
	private slots:
	void HandleConnection();
	void HandleDisconnect();
	void HandleMessage();
};