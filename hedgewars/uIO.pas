(*
 * Hedgewars, a free turn based strategy game
 * Copyright (c) 2004-2015 Andrey Korotaev <unC0Rr@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *
 * This file was modified by Matthias Hennemann <hennemann.matthias@gmail.com> on
 * 17/07/2016
 *
 *)

{$INCLUDE "options.inc"}

unit uIO;
interface
uses SDLh, uTypes, sysUtils, classes;

procedure initModule;
procedure freeModule;

// My own procedures
procedure SendIPCGC(s: shortstring);
procedure ProcessCommands; 
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ; 
procedure ClearActions;
function GetArbitraryHogOfTeam(team : PTeam) : PHedgehog; 
function GetArbitraryHogIdOfTeam(team: PTeam) : Longword;
function GetArbitraryHogNameOfTeam(team: PTeam) : string;
function GetHogIdByName(name: string) : Longword; 
function GetHogByName(name: string) : PHedgehog;
function GetTeamByName(name: string) : PTeam;
procedure SwitchGamePhase;
procedure EnterChooseWeaponPhase;


procedure InitIPC;
procedure SendIPC(s: shortstring);
procedure SendIPCXY(cmd: char; X, Y: LongInt);
procedure SendIPCRaw(p: pointer; len: Longword);
procedure SendIPCAndWaitReply(s: shortstring);
procedure FlushMessages(Lag: Longword);
procedure LoadRecordFromFile(fileName: shortstring);
procedure SendStat(sit: TStatInfoType; s: shortstring);
procedure IPCWaitPongEvent;
procedure IPCCheckSock;
procedure NetGetNextCmd;
procedure doPut(putX, putY: LongInt; fromAI: boolean);

var showCoordGrid : boolean = false;
var showAngleGrid : boolean = false;
var isMovePhase : boolean = true;
var isNextAirAttackLeft : boolean = true;
var lastAmmoTypeChosen : TAmmoType = amGrenade;
var lastShopAmmo : TAmmoType = amGrenade;

var luaCommandListBetweenTurns : TStringList;
var cheerMessageList : TStringList;

implementation
uses uConsole, uConsts, uVariables, uCommands, uUtils, uDebug, uScript, uRenderUtils, uRender, uCommandHandlers, 
uAIActions, uGearsList, uFloat, uWorld, uCaptions, uAmmos, typinfo;

const
    cSendEmptyPacketTime = 1000;
    cSendBufferSize = 1024;

type PCmd = ^TCmd;
     TCmd = packed record
            Next: PCmd;
            loTime: Word;
            case byte of
            1: (len: byte;
                cmd: Char);
            2: (str: shortstring);
            end;		

var IPCSock: PTCPSocket;
    fds: PSDLNet_SocketSet;
    isPonged: boolean;
    SocketString: shortstring;
	commandActions : TActions; 		

    headcmd: PCmd;
    lastcmd: PCmd;

    flushDelayTicks: LongWord;
    sendBuffer: record
                buf: array[0..Pred(cSendBufferSize)] of byte;
                count: Word;
                end;

function AddCmd(Time: Word; str: shortstring): PCmd;
var command: PCmd;
begin
new(command);
FillChar(command^, sizeof(TCmd), 0);
command^.loTime:= Time;
command^.str:= str;
if (command^.cmd <> 'F') and (command^.cmd <> 'G') then dec(command^.len, 2); // cut timestamp
if headcmd = nil then
    begin
    headcmd:= command;
    lastcmd:= command
    end
else
    begin
    lastcmd^.Next:= command;
    lastcmd:= command
    end;
AddCmd:= command;
end;

procedure RemoveCmd;
var tmp: PCmd;
begin
TryDo(headcmd <> nil, 'Engine bug: headcmd = nil', true);
tmp:= headcmd;
headcmd:= headcmd^.Next;
if headcmd = nil then
    lastcmd:= nil;
dispose(tmp)
end;

procedure InitIPC;
var ipaddr: TIPAddress;
begin
	luaCommandListBetweenTurns := TStringList.Create;
	cheerMessageList := TStringList.Create;
    WriteToConsole('Init SDL_Net... ');
    SDLTry(SDLNet_Init = 0, true);
    fds:= SDLNet_AllocSocketSet(1);
    SDLTry(fds <> nil, true);
    WriteLnToConsole(msgOK);
    WriteToConsole('Establishing IPC connection to tcp 127.0.0.1:' + IntToStr(ipcPort) + ' ');
    {$HINTS OFF}
    SDLTry(SDLNet_ResolveHost(ipaddr, PChar('127.0.0.1'), ipcPort) = 0, true);
    {$HINTS ON}
    IPCSock:= SDLNet_TCP_Open(ipaddr);
    SDLTry(IPCSock <> nil, true);
    WriteLnToConsole(msgOK)
end;

procedure ParseChatCommand(command: shortstring; message: shortstring;
                           messageStartIndex: Byte);
var
    text: shortstring;
begin
    text:= copy(message, messageStartIndex,
                Length(message) - messageStartIndex + 1);
    ParseCommand(command + text, true);
    WriteLnToConsole(text)
end;

procedure ParseIPCCommand(s: shortstring);
var loTicks: Word;
    isProcessed: boolean;
	arg1: LongInt;
	arg2: LongInt;
	arg3: LongInt;
	stringArg1: string;
	stringArg2: string;
	stringTest: string;
	splitList : TStringList;
	breakAfterJump: LongInt = 2000;
begin
isProcessed := true;

case s[1] of
     '!': begin AddFileLog('Ping? Pong!'); isPonged:= true; end;
     '?': SendIPC(_S'!');
     'e': ParseCommand(copy(s, 2, Length(s) - 1), true);
     'E': OutError(copy(s, 2, Length(s) - 1), true);
     'W': OutError(copy(s, 2, Length(s) - 1), false);
     'M': ParseCommand('landcheck ' + s, true);
     'o': if fastUntilLag then ParseCommand('forcequit', true);
     'T': case s[2] of
               'L': GameType:= gmtLocal;
               'D': GameType:= gmtDemo;
               'N': GameType:= gmtNet;
               'S': GameType:= gmtSave;
               'V': GameType:= gmtRecord;
               else OutError(errmsgIncorrectUse + ' IPC "T" :' + s[2], true) end;
     'V': begin
              if s[2] = '.' then
                  ParseCommand('campvar ' + copy(s, 3, length(s) - 2), true);
          end;
     'I': ParseCommand('pause server', true);
     's': if gameType = gmtNet then
             ParseChatCommand('chatmsg ', s, 2)
          else
             isProcessed:= false;
     'b': if gameType = gmtNet then
             ParseChatCommand('chatmsg ' + #4, s, 2)
          else
             isProcessed:= false;
     'Y': ChatPasteBuffer:= copy(s, 2, Length(s) - 1);
	 'v': begin // These are commands initiated by game controller
			AddFileLog('[GC In] '+ s);
			case s[2] of
				'A': begin // Set Target (Aim) -> Show coordinate grid
					AddFileLog('[TPHW] CoordGrid ON');
					showCoordGrid := true;
					end;
				'B': begin // Set Target
					AddFileLog('[TPHW Target] Target Command: ' + BoolToStr(showCoordGrid));
					if(showCoordGrid) then
					begin
						case s[3] of
							'N': begin // Normal Target setting (e.g. for Bee, Piano)
									// Split complete argument string in two
									splitList := TStringList.Create;
									Split(' ', copy(s, 5, length(s) - 4), splitList);
							
									// Get X coordinate
									arg1:= StrToInt(splitList[0]) * 10;
									
									// Get Y coordinate
									arg2:= StrToInt(splitList[1]) * 10;
									
									// Trigger action
									AddAction(commandActions, aia_Put, 0, 1, arg1, arg2)
								end;
							'D': begin // Target setting with direction (e.g. AirStrike -> Left/Right)
									// Split complete argument string in two
									splitList := TStringList.Create;
									Split(' ', copy(s, 5, length(s) - 4), splitList);
							
									// Get X coordinate
									arg1:= StrToInt(splitList[0]) * 10;
									
									// Get Y coordinate
									arg2:= StrToInt(splitList[1]) * 10;
									
									// Get direction
									stringArg1:= splitList[2];
									AddFileLog('[TPHW Target] Direction ' + stringArg1);
									
									// Trigger action
									case stringArg1[1] of 
										'l': begin	// Plane should come from left side
											isNextAirAttackLeft := true;
											// Nothing to do, plane is initially coming from left side
											end;
										'r': begin	// Plane should come from right side
											isNextAirAttackLeft := false;
											//AddAction(commandActions,  aia_Left , 0, 0, 0, 0);
											//AddAction(commandActions,  aia_none , 0, 2, 0, 0);
											end;											
									end;
									AddAction(commandActions, aia_Put, 0, 100, arg1, arg2)
								end;
							'S': begin // Target setting with direction and setting (e.g. Drillstrike)
									// Split complete argument string in two
									splitList := TStringList.Create;
									Split(' ', copy(s, 5, length(s) - 4), splitList);
							
									// Get X coordinate
									arg1:= StrToInt(splitList[0]) * 10;
									
									// Get Y coordinate
									arg2:= StrToInt(splitList[1]) * 10;
									
									// Get Direction
									stringArg1:= splitList[2];
									
									// Get Settings
									arg3:= StrToInt(splitList[3]);
									
									// Trigger action
									case stringArg1[1] of 
										'l': begin	// Plane should come from left side
											isNextAirAttackLeft := true;
											// Nothing to do, plane is initially coming from left side
											end;
										'r': begin	// Plane should come from right side
											isNextAirAttackLeft := false;
											//AddAction(commandActions,  aia_Left , 0, 0, 0, 0);
											//AddAction(commandActions,  aia_none , 0, 2, 0, 0);
											end;											
									end;
									AddAction(commandActions, aia_Timer, arg3, 0, 0, 0);
									AddAction(commandActions, aia_Put, 0, 100, arg1, arg2)
								end;
						end;
						AddFileLog('[TPHW] CoordGrid OFF');
						showCoordGrid := false; // Hide grid again
						autoCameraOn := true;
						FollowGear:= CurrentHedgehog^.Gear;
						SwitchGamePhase();
						end;
					end;
				'C':begin // Shop Item
					case s[3] of
						'1': begin // Raise Water
							// LuaParseString('SetWaterLine(WaterLine + 20)'); // Increase Waterlevel instant without animation
							// bWaterRising := true; // Permanent water rising
							stringArg1 := copy(s, 5, length(s) - 4);
							luaCommandListBetweenTurns.Add('C ' + stringArg1 + ' has risen the water level!');
							luaCommandListBetweenTurns.Add('F');							
							luaCommandListBetweenTurns.Add('W'); 
							end;
						'2': begin // First Aid Crate
							AddFileLog('[GC Debbug] HC start');
							bBetweenTurns := true;
							stringArg1 := copy(s, 5, length(s) - 4);
							luaCommandListBetweenTurns.Add('A ' + stringArg1); 
							end;
						'3': begin // Light Weapon Crate
							AddFileLog('[GC Debbug] LWC start');
							stringArg1 := copy(s, 5, length(s) - 4);
							luaCommandListBetweenTurns.Add('B ' + stringArg1); 
							end;
						'4': begin // Heavy Weapon Crate
							AddFileLog('[GC Debbug] HWC start');
							stringArg1 := copy(s, 5, length(s) - 4);
							luaCommandListBetweenTurns.Add('D ' + stringArg1); 
							end;
						'5': begin // Poison
							stringArg1 := copy(s, 5, length(s) - 4);
							luaCommandListBetweenTurns.Add('E ' + stringArg1); 
							end;
						'6': begin // Resurrect hog
							//splitList := TStringList.Create;
							//Split(' ', copy(s, 5, length(s) - 4), splitList);
							//stringArg1 := splitList[0];

							///luaCommandListBetweenTurns.Add('L FindPlace(AddHog("TestHog", 0, 100, "NoHat"), false, 0, LAND_WIDTH)');
							//AddFileLog('[GC Debbug] Resurrect Filling command list');
							//luaCommandListBetweenTurns.Add('R ' + stringArg1);
							//luaCommandListBetweenTurns.Add('L SetEffect(' + IntToStr(GetHogIdByName(stringArg1)) + ', heResurrectable, 1)');
							//luaCommandListBetweenTurns.Add('C Reinforcements arrived!'); 
							//luaCommandListBetweenTurns.Add('L AddCaption("Reinforcements arrived!", 0xFFFFFF, tphwMessage)'); 
							//AddFileLog('[GC Debbug] Resurrect end');
							luaCommandListBetweenTurns.Add('C Resurrection!'); 
							luaCommandListBetweenTurns.Add('W');
							end; 
						'7': begin // Assassinate hog
							stringArg1 := copy(s, 5, length(s) - 4);
							luaCommandListBetweenTurns.Add('G ' + stringArg1);							
							end;
						'8': begin // In game message
							stringArg1 := copy(s, 5, length(s) - 4);
							splitList := TStringList.Create;
							Split(' ', stringArg1, splitList);
							stringArg2 := splitList[0];
							stringArg1 :=  copy(stringArg1, length(stringArg2) + 1, length(stringArg1));
							cheerMessageList.Add(stringArg2 + ': ' + stringArg1);
							end;
					end;
					end;
				'D':begin // Double Jump
						if(NOT bShowAmmoMenu) then 
						begin
							with CurrentHedgehog^ do begin
								AddAction(commandActions, aia_HJump, 0, 0, 0, 0);
								AddAction(commandActions, aia_HJump, 0, 150, 0, 0);
								AddAction(commandActions, aia_none, 0, breakAfterJump, 0, 0);
							end;
						end;
					end;
				'E':begin // ShowAngleGrid
						AddFileLog('[TPHW] AngleGrid ON');
						showAngleGrid := true;
					end;
				'F': begin // Fire
					if(NOT isMovePhase) then
					begin 
						case s[3] of
							'1': begin  // Just Fire
									AddAction(commandActions, aia_attack, aim_push, 300, 0, 0);
									AddAction(commandActions, aia_attack, aim_release, 1, 0, 0);
								end;
							'2': begin 	// Fire Angle Power
									// Split complete argument string in two
									splitList := TStringList.Create;
									Split(' ', copy(s, 5, length(s) - 4), splitList);
							
									// Get Angle
									arg1:= StrToInt(splitList[0]);
									
									// Get Firepower
									arg2:= StrToInt(splitList[1]);
							
									// OutError(errmsgIncorrectUse + ' IPC "fire move" :' + IntToStr(arg1) + ' ' + IntToStr(arg2) + ' Input: ' +  copy(s, 3, length(s) - 2), true); //Debug
									with CurrentHedgehog^ do begin
										// Adjust angle for 360
										if(arg1 > 180) then 
										begin
											AddFileLog('[TPHW Angle] Angle bigger 180');
											arg1 := 360 - arg1;
											AddFileLog('[TPHW Angle] New Angle is ' + IntToStr(arg1));
											if(CurrentHedgehog^.Gear^.dX.isNegative) then
											begin
												// left
												AddFileLog('[TPHW Angle] Looking right, turning to left');
												AddAction(commandActions,  aia_LookRight , 0, 1, 0, 0);
											end else 
											begin
												// right
												AddFileLog('[TPHW Angle] Looking left, turning to right');
												AddAction(commandActions,  aia_LookLeft , 0, 1, 0, 0);
											end;
										end;
									
										// Adjust angle and firepower
										AddFileLog('[TPHW Angle] Now normal firing');
										AddAction(commandActions, aia_Lua, 0, 1, arg1, arg2);
										AddAction(commandActions, aia_attack, aim_push, 300, 0, 0);
										AddAction(commandActions, aia_attack, aim_release, 1, 0, 0);
										// AddAction(commandActions, aia_attack, 0, 0, 0, 0);
									end;
								end;
							'3': begin 	// Fire Angle
									// Get Angle
									arg1 := StrToInt(copy(s, 5, length(s) - 4));
							
									// OutError(errmsgIncorrectUse + ' IPC "fire move" :' + IntToStr(arg1) + ' ' + IntToStr(arg2) + ' Input: ' +  copy(s, 3, length(s) - 2), true); //Debug
									with CurrentHedgehog^ do begin
										// Adjust angle for 360
										if(arg1 > 180) then 
										begin
											AddFileLog('[TPHW Angle] Angle bigger 180');
											arg1 := 360 - arg1;
											AddFileLog('[TPHW Angle] New Angle is ' + IntToStr(arg1));
											if(CurrentHedgehog^.Gear^.dX.isNegative) then
											begin
												// left
												AddFileLog('[TPHW Angle] Looking right, turning to left');
												AddAction(commandActions,  aia_LookRight , 0, 1, 0, 0);
											end else 
											begin
												// right
												AddFileLog('[TPHW Angle] Looking left, turning to right');
												AddAction(commandActions,  aia_LookLeft , 0, 1, 0, 0);
											end;
										end;									
								
										// Adjust angle
										AddAction(commandActions, aia_Lua, 0, 1, arg1, -1);
										//LuaParseString('SetGearValues(CurrentHedgehog, ' + IntToStr(round((arg1 / 360.0) * 4096.0)) + ', nil, nil, nil, nil, nil, nil, nil, nil, nil, 0xFFFFFFFF)'); 
										AddAction(commandActions, aia_attack, aim_push, 0, 0, 0);
										AddAction(commandActions, aia_attack, aim_release, 1, 0, 0);
										// AddAction(commandActions, aia_attack, 0, 0, 0, 0);
									end;
								end;
							'4': begin 	// Fire Angle Power Settings
									// Split complete argument string in three
									splitList := TStringList.Create;
									Split(' ', copy(s, 5, length(s) - 4), splitList);
							
									// Get Angle
									arg1:= StrToInt(splitList[0]);
									
									// Get Firepower
									arg2:= StrToInt(splitList[1]);
									
									// Get Settings
									arg3:= StrToInt(splitList[2]);
							
									// OutError(errmsgIncorrectUse + ' IPC "fire move" :' + IntToStr(arg1) + ' ' + IntToStr(arg2) + ' Input: ' +  copy(s, 3, length(s) - 2), true); //Debug
									with CurrentHedgehog^ do begin
										// Adjust angle for 360
										if(arg1 > 180) then 
										begin
											AddFileLog('[TPHW Angle] Angle bigger 180');
											arg1 := 360 - arg1;
											AddFileLog('[TPHW Angle] New Angle is ' + IntToStr(arg1));
											if(CurrentHedgehog^.Gear^.dX.isNegative) then
											begin
												// left
												AddFileLog('[TPHW Angle] Looking right, turning to left');
												AddAction(commandActions,  aia_LookRight , 0, 1, 0, 0);
											end else 
											begin
												// right
												AddFileLog('[TPHW Angle] Looking left, turning to right');
												AddAction(commandActions,  aia_LookLeft , 0, 1, 0, 0);
											end;
										end;	
									
										// Adjust angle and firepower
										AddAction(commandActions, aia_Lua, 0, 1, arg1, arg2);
										AddAction(commandActions, aia_Timer, arg3, 1, 0, 0);
										AddAction(commandActions, aia_attack, aim_push, 300, 0, 0);
										AddAction(commandActions, aia_attack, aim_release, 1, 0, 0);
										// AddAction(commandActions, aia_attack, 0, 0, 0, 0);
									end;
								end;
							'5': begin 	// Fire Settings
									// Get Settings
									arg3:= StrToInt(copy(s, 5, length(s) - 4));
							
									// OutError(errmsgIncorrectUse + ' IPC "fire move" :' + IntToStr(arg1) + ' ' + IntToStr(arg2) + ' Input: ' +  copy(s, 3, length(s) - 2), true); //Debug
									with CurrentHedgehog^ do begin
										AddAction(commandActions, aia_Timer, arg3, 0, 0, 0);
										AddAction(commandActions, aia_attack, aim_push, 300, 0, 0);
										AddAction(commandActions, aia_attack, aim_release, 1, 0, 0);
										// AddAction(commandActions, aia_attack, 0, 0, 0, 0);
									end;
								end;
							'6': begin 	// Fire Timer
									// Get Timer
									arg3:= StrToInt(copy(s, 5, length(s) - 4));
							
									// OutError(errmsgIncorrectUse + ' IPC "fire move" :' + IntToStr(arg1) + ' ' + IntToStr(arg2) + ' Input: ' +  copy(s, 3, length(s) - 2), true); //Debug
									AddAction(commandActions, aia_attack, aim_push, 300, 0, 0);
									AddAction(commandActions, aia_attack, aim_release, 100, 0, 0);
									AddAction(commandActions, aia_attack, aim_push, arg3 * 1000, 0, 0);
									AddAction(commandActions, aia_attack, aim_release, 100, 0, 0);
								end;
							'7': begin 	// Fire Angle Timer
									// Split complete argument string in three
									splitList := TStringList.Create;
									Split(' ', copy(s, 5, length(s) - 4), splitList);
							
									// Get Angle
									arg1:= StrToInt(splitList[0]);
									
									// Get Timer
									arg2:= StrToInt(splitList[1]);
							
									// OutError(errmsgIncorrectUse + ' IPC "fire move" :' + IntToStr(arg1) + ' ' + IntToStr(arg2) + ' Input: ' +  copy(s, 3, length(s) - 2), true); //Debug
									with CurrentHedgehog^ do begin
										// Adjust angle for 360
										if(arg1 > 180) then 
										begin
											AddFileLog('[TPHW Angle] Angle bigger 180');
											arg1 := 360 - arg1;
											AddFileLog('[TPHW Angle] New Angle is ' + IntToStr(arg1));
											if(CurrentHedgehog^.Gear^.dX.isNegative) then
											begin
												// left
												AddFileLog('[TPHW Angle] Looking right, turning to left');
												AddAction(commandActions,  aia_LookRight , 0, 1, 0, 0);
											end else 
											begin
												// right
												AddFileLog('[TPHW Angle] Looking left, turning to right');
												AddAction(commandActions,  aia_LookLeft , 0, 1, 0, 0);
											end;
										end;	
									
										// Adjust angle
										AddAction(commandActions, aia_Lua, 0, 1, arg1, -1); 
										AddAction(commandActions, aia_attack, aim_push, 300, 0, 0);
										AddAction(commandActions, aia_attack, aim_release, 100, 0, 0);
										AddAction(commandActions, aia_attack, aim_push, arg2 * 1000, 0, 0);
										AddAction(commandActions, aia_attack, aim_release, 100, 0, 0);
									end;
								end;
							'8': begin // Fire Target

								end;
							'9': begin // Fire Target Settings
							
								end;
							end;
					end;
					end;
				'H': begin // High Jump
						if(NOT bShowAmmoMenu) then 
						begin
							with CurrentHedgehog^ do begin
								AddAction(commandActions, aia_HJump, 0, 0, 0, 0);
								AddAction(commandActions, aia_none, 0, breakAfterJump, 0, 0);
							end;
						end;
					end;
				'J': begin // (Low) Jump
						if(NOT bShowAmmoMenu) then 
						begin
							with CurrentHedgehog^ do begin	
								AddAction(commandActions, aia_LJump, 0, 1, 0, 0);
								AddAction(commandActions, aia_none, 0,  breakAfterJump, 1, 0);
							end;
						end;
					end;
				'L':begin // Left
						if(NOT bShowAmmoMenu) then 
						begin
							arg1:= StrToInt(copy(s, 4, length(s) - 3));
							with CurrentHedgehog^ do begin
								AddAction(commandActions,  aia_Left , 0, 0, 0, 0);
								AddAction(commandActions,  aia_LookLeft , 0, arg1 * 300, 0, 0);
								// OutError(errmsgIncorrectUse + ' IPC "left move" :' + IntToStr(arg1) + ' ' + test + ' Testende ' + s, true); //Debug
							end;
						end;
					end;
				'Q': begin // Quit Move Phase
						with CurrentHedgehog^ do begin
								AddAction(commandActions,  aia_Stop, 0, 0, 0, 0);
								// OutError(errmsgIncorrectUse + ' IPC "left move" :' + IntToStr(arg1) + ' ' + test + ' Testende ' + s, true); //Debug
						end;
					end;
				'R': begin // right
						if(NOT bShowAmmoMenu) then 
						begin
							arg1:= StrToInt(copy(s, 4, length(s) - 3));
							with CurrentHedgehog^ do begin
								AddAction(commandActions,  aia_Right , 0, 0, 0, 0);
								AddAction(commandActions,  aia_LookRight , 0, arg1 * 300, 0, 0);
								// OutError(errmsgIncorrectUse + ' IPC "left move" :' + IntToStr(arg1) + ' ' + ' Testende ' + s, true); //Debug
							end;
						end;
					end;
				'S': begin // Execute argument as lua string
						stringArg1 := copy(s, 4, length(s) - 3);
						LuaParseString(stringArg1); 
						// OutError(errmsgIncorrectUse + ' ' + ' Lua Test ' + s, true); // Debug
					end;
				'T': begin // Test Scenario
						LuaParseString('SwitchHog(' + IntToStr(GetHogIdByName('Adrian')) + ')'); // Switch Hedgehog
					end;
				'W': begin // Switch Worm
						stringArg1 := copy(s, 4, length(s) - 3);
						LuaParseString('SwitchHog(' + IntToStr(GetHogIdByName(stringArg1)) + ')'); // Switch Hedgehog
						FollowGear := CurrentHedgehog^.Gear;
						//OutError(errmsgIncorrectUse + ' IPC "vW" Tried switch to hog: ' + stringArg1, true);
					end;
				'Z':begin // Zoom
					// Parse argument
					arg1:= StrToInt(copy(s, 5, length(s) - 4));
					case s[3] of 
					   'i': begin // Zoom In
								while arg1 > 0 do 
									begin
									chZoomIn(s); 
									Dec(arg1);
									end;
							end;
					   'o': begin // Zoom Out
								while arg1 > 0 do 
									begin
									chZoomOut(s); 
									Dec(arg1);
									end;
							end;
						else OutError(errmsgIncorrectUse + ' IPC "vZ" :' + s[3], true) end;
					end;					
				else OutError(errmsgIncorrectUse + ' IPC "v" :' + s[2], true); end;
			end;
	 
		//with CurrentHedgehog^.Gear^ do 
		//	begin
		//		DrawTextureCentered(0, 8, RenderStringTex('Hello', cWhiteColor, fntBig));
		//	end
				// LuaParseString('AddCaption(''Some Event happened!'', 0xFFFFFF, capgrpGameState)'); 
				// LuaParseString('AddCaption(''AmmoInfo!'', 0xFFFFFF, capgrpAmmoinfo)'); 
				// LuaParseString('AddCaption(''Volume!'', 0xFFFFFF, capgrpVolume)'); 
				// LuaParseString('AddCaption(''Message!'', 0xFFFFFF, capgrpMessage)'); 
				// LuaParseString('AddCaption(''Message2!'', 0xFFFFFF, capgrpMessage2)'); 
				// LuaParseString('AddCaption(''AmmoState!'', 0xFFFFFF, capgrpAmmostate)'); 
				// LuaParseString('SetGearValues(CurrentHedgehog, nil, 1000, nil, nil, nil, nil, nil, nil, nil, nil, 0xFFFFFFFF)'); // Adjust fire power (Max is 1500)
				// LuaParseString('SetGearValues(CurrentHedgehog, 1024, nil, nil, nil, nil, nil, nil, nil, nil, nil, 0xFFFFFFFF)'); // Adjust angle (Max Angle is 2048)
				// LuaParseString('SetHealth(CurrentHedgehog, 150)'); // Change Health of Hedgehog
				// LuaParseString('SetHogHat(CurrentHedgehog, NoHat)'); // Change Hat of Hedgehog
				// LuaParseString('SetHogName(CurrentHedgehog, ''Hans Peter'')'); // Rename Hedgehog
				// LuaParseString('SpawnAmmoCrate(GetX(CurrentHedgehog), GetY(CurrentHedgehog) - 30, amGrenade)'); // Spawn Ammo Crate
				// LuaParseString('SetWind(100)');  // Set Windspeed
				// LuaParseString('SetWeapon(amBazooka)');  // Set Weapon to bazooka
				// LuaParseString('SetWaterLine(WaterLine + 20)'); // Increase Waterlevel instant without animation
				// bWaterRising := true; // Permanent water rising
				// AddGear(0, 0, gtWaterUp, 0, _0, _0, 0)^.Tag:= cWaterRise; // One step of water rising
				
				// Outdated
				// Message:= Message or (gmAttack and InputMask); // Start building Fire Power
				// Message:= Message and (not (gmAttack and InputMask)); // Fire
				// SpawnCustomCrateAt(GetGearPosition(CurrentHedgehog^.Gear^.Pos^.x), AmmoCrate, 5, 0);  // Not working
				// SpawnFakeCrateAt(100, 100, AmmoCrate, false, false); // Probably not working

	 // exit;
	 // CurrentHedgehog^.Gear^.UID
     else
        isProcessed:= false;
     end;

    if (not isProcessed) then
    begin
        loTicks:= SDLNet_Read16(@s[byte(s[0]) - 1]);
        AddCmd(loTicks, s);
        AddFileLog('[IPC in] ' + sanitizeCharForLog(s[1]) + ' ticks ' + IntToStr(lastcmd^.loTime));
    end
end;

function GetArbitraryHogOfTeam(team : PTeam) : PHedgehog;
var hog : THedgehog;
begin
	AddFileLog('[GC Debbug] Running over all hogs of team ');
	for hog in team^.Hedgehogs do
		begin
			if(hog.Gear <> nil) then 
				begin 
				GetArbitraryHogOfTeam := @hog;
				exit;
				end;
		end;
	GetArbitraryHogOfTeam := nil;
end;

function GetArbitraryHogIdOfTeam(team: PTeam) : Longword;
var hog : THedgehog;
begin
	AddFileLog('[GC Debbug] Running over all hogs of team ');
	for hog in team^.Hedgehogs do
		begin
			if(hog.Gear <> nil) then 
				begin 
				AddFileLog('[GC Debbug] First hog with enough hp found: ' + hog.Name);
				GetArbitraryHogIdOfTeam := hog.Gear^.uid;
				AddFileLog('[GC Debbug] Hog foudn with ' + IntToStr(hog.HealthBarHealth) + ' hp');
				exit;
				end;
		end;
end;

function GetArbitraryHogNameOfTeam(team: PTeam) : string;
var hog : THedgehog;
begin
	for hog in team^.Hedgehogs do
		begin
			if(hog.Gear <> nil) then 
				begin
				GetArbitraryHogNameOfTeam := hog.Name;
				AddFileLog('[GC Debbug] Hog foudn with ' + IntToStr(hog.HealthBarHealth) + ' hp');
				exit;
				end;
		end;
end;

function GetHogIdByName(name: string) : Longword; 
var team : Pteam;
	hog : THedgehog;
begin
	hog := GetHogByName(name)^;
	if (hog.Gear = nil) then
		begin
		GetHogIdByName := 0;
		exit;
		end;
	GetHogIdByName := hog.Gear^.uid;
end;

function GetHogByName(name: string) : PHedgehog;
var team : Pteam;
	hog: THedgehog;
begin 
	for team in TeamsArray do 
		begin
			for hog in team^.Hedgehogs do
			begin
				if(name = hog.Name) then 
					begin
					GetHogByName := @hog;
					exit;
					end;
			end;
		end;
	GetHogByName := nil;
end;

function GetTeamByName(name: string) : PTeam;
begin
if(name = PreviousTeam^.TeamName) then
	begin
	GetTeamByName := PreviousTeam;
	exit;
	end;
GetTeamByName := CurrentTeam;
end;

procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ; 
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.DelimitedText   := Str;
end;

// Resets the round time when a phase switches
procedure SwitchGamePhase;
begin
// Reset timer
TurnTimeLeft := cHedgehogTurnTime;
end;

procedure EnterChooseWeaponPhase;
begin
SwitchGamePhase();
ClearActions();
// Display Weapon window
AddFileLog('[TPHW] AmmoMenu ON');
bShowAmmoMenu := true;
isMovePhase := false;
ClearActions();

// Inform GC
SendIPCGC('M');
end;

procedure ProcessCommands; 
begin
with CurrentHedgehog^ do
    if (Gear <> nil)
    and ((Gear^.State and gstHHDriven) <> 0)
    and (TurnTimeLeft < cHedgehogTurnTime - 50) then
            if (commandActions.Pos >= commandActions.Count) then
                begin
                if Gear^.Message <> 0 then
                    begin
                    StopMessages(Gear^.Message);
                    TryDo((Gear^.Message and gmAllStoppable) = 0, 'Engine bug: AI may break demos playing', true);
                    end;
                if Gear^.Message <> 0 then
                    exit;

            end else
                begin
                ProcessAction(commandActions, Gear)
                end
end;

procedure ClearActions; 
begin
	AddFileLog('[Own Internal] Actions Cleared');
	commandActions.Count:=0;
	commandActions.Pos:=0;
	commandActions.Score:=0;
end;

procedure IPCCheckSock;
var i: LongInt;
    s: shortstring;
begin
    if IPCSock = nil then
        exit;

    fds^.numsockets:= 0;
    SDLNet_AddSocket(fds, IPCSock);

    while SDLNet_CheckSockets(fds, 0) > 0 do
    begin
        i:= SDLNet_TCP_Recv(IPCSock, @s[1], 255 - Length(SocketString));
        if i > 0 then
        begin
            s[0]:= char(i);
            SocketString:= SocketString + s;
            while (Length(SocketString) > 1) and (Length(SocketString) > byte(SocketString[1])) do
            begin
                ParseIPCCommand(copy(SocketString, 2, byte(SocketString[1])));
                Delete(SocketString, 1, Succ(byte(SocketString[1])))
            end
        end
    else
        OutError('IPC connection lost', true)
    end;
end;

procedure LoadRecordFromFile(fileName: shortstring);
var f  : File;
    ss : shortstring = '';
    i  : LongInt;
    s  : shortstring;
begin

// set RDNLY on file open
filemode:= 0;
{$I-}
assign(f, fileName);
reset(f, 1);
tryDo(IOResult = 0, 'Error opening file ' + fileName, true);

i:= 0; // avoid compiler hints
s[0]:= #0;
repeat
    BlockRead(f, s[1], 255 - Length(ss), i);
    if i > 0 then
        begin
        s[0]:= char(i);
        ss:= ss + s;
        while (Length(ss) > 1)and(Length(ss) > byte(ss[1])) do
            begin
            ParseIPCCommand(copy(ss, 2, byte(ss[1])));
            Delete(ss, 1, Succ(byte(ss[1])));
            end
        end
until i = 0;

close(f)
{$I+}
end;

procedure SendStat(sit: TStatInfoType; s: shortstring);
const stc: array [TStatInfoType] of char = ('r', 'D', 'k', 'K', 'H', 'T', 'P', 's', 'S', 'B', 'c', 'g', 'p');
var buf: shortstring;
	ammoHedgehog : PHedgehog;
begin
buf:= 'i' + stc[sit] + s;
if(stc[sit] = 'H') then 
begin
with CurrentHedgehog^ do
    if (Gear <> nil) then
			begin
			if Gear^.Message <> 0 then
				begin
				StopMessages(Gear^.Message);
				TryDo((Gear^.Message and gmAllStoppable) = 0, 'Engine bug: AI may break demos playing', true);
				end;
			end;
end;	
// Clear commands for next turn
commandActions.Count:=0;
commandActions.Pos:=0;
commandActions.Score:=0;
AddFileLog('[TPHW] CoordGrid OFF');
showCoordGrid := false;
AddFileLog('[TPHW] AngleGrid OFF');
showAngleGrid := false;
autoCameraOn := true;
AddFileLog('[TPHW] AmmoMenu OFF');
bShowAmmoMenu := false;
// Get ammunition of chosen weapon
ammoHedgehog := GetArbitraryHogOfTeam(CurrentTeam);
if(ammoHedgehog <> nil) then
begin 
	SendIPCGC('A' + CurrentTeam^.TeamName + ' ' + GetEnumName(TypeInfo(TAmmoType), Ord(lastAmmoTypeChosen)) + ' ' + IntToStr(GetAmmoEntry(ammoHedgehog^, lastAmmoTypeChosen)^.Count));
end;
			
SendIPCRaw(@buf[0], length(buf) + 1)
end;

function isSyncedCommand(c: char): boolean;
begin
    case c of
    '+', '#', 'L', 'l', 'R', 'r', 'U'
    , 'u', 'D', 'd', 'Z', 'z', 'A', 'a'
    , 'S', 'j', 'J', ',', 'c', 'N', 'p'
    , 'P', 'w', 't', '1', '2', '3', '4'
    , '5', 'f', 'g': isSyncedCommand:= true;
    else
        isSyncedCommand:= ((byte(c) >= 128) and (byte(c) <= 128 + cMaxSlotIndex))
    end
end;

procedure flushBuffer();
begin
    if IPCSock <> nil then
        begin
        SDLNet_TCP_Send(IPCSock, @sendBuffer.buf, sendBuffer.count);
        flushDelayTicks:= 0;
        sendBuffer.count:= 0
        end
end;

procedure SendIPCGC(s: shortstring);
begin
SendIPC('B' + s);
AddFileLog('[GC Out] ' + s);
end;

procedure SendIPC(s: shortstring);
begin
if IPCSock <> nil then
    begin
    if s[0] > #251 then
        s[0]:= #251;

    SDLNet_Write16(GameTicks, @s[Succ(byte(s[0]))]);

    AddFileLog('[IPC out] '+ sanitizeCharForLog(s[1]));
    inc(s[0], 2);

    if isSyncedCommand(s[1]) then
        begin
        if sendBuffer.count + byte(s[0]) >= cSendBufferSize then
            flushBuffer();

        Move(s, sendBuffer.buf[sendBuffer.count], byte(s[0]) + 1);
        inc(sendBuffer.count, byte(s[0]) + 1);

        if (s[1] = 'N') or (s[1] = '#') then
            flushBuffer();
        end else
        SDLNet_TCP_Send(IPCSock, @s, Succ(byte(s[0])))
    end
end;

procedure SendIPCRaw(p: pointer; len: Longword);
begin
if IPCSock <> nil then
    begin
    SDLNet_TCP_Send(IPCSock, p, len)
    end
end;

procedure SendIPCXY(cmd: char; X, Y: LongInt);
var s: shortstring;
begin
s[0]:= #9;
s[1]:= cmd;
SDLNet_Write32(X, @s[2]);
SDLNet_Write32(Y, @s[6]);
SendIPC(s)
end;

procedure IPCWaitPongEvent;
begin
isPonged:= false;
repeat
    IPCCheckSock;
    SDL_Delay(1)
until isPonged
end;

procedure SendIPCAndWaitReply(s: shortstring);
begin
SendIPC(s);
SendIPC(_S'?');
IPCWaitPongEvent
end;

procedure FlushMessages(Lag: Longword);
begin
inc(flushDelayTicks, Lag);
if (flushDelayTicks >= cSendEmptyPacketTime) then
    begin
    if sendBuffer.count = 0 then
        SendIPC(_S'+');

     flushBuffer()
    end
end;

procedure NetGetNextCmd;
var tmpflag: boolean;
    s: shortstring;
    x32, y32: LongInt;
begin
tmpflag:= true;

while (headcmd <> nil)
    and (tmpflag or (headcmd^.cmd = '#')) // '#' is the only cmd which can be sent within same tick after 'N'
    and ((GameTicks = LongWord(hiTicks shl 16 + headcmd^.loTime))
        or (headcmd^.cmd = 's') // for these commands time is not specified
        or (headcmd^.cmd = 'h') // seems the hedgewars protocol does not allow remote synced commands
        or (headcmd^.cmd = '#') // must be synced for saves to work
        or (headcmd^.cmd = 'b')
        or (headcmd^.cmd = 'F')
        or (headcmd^.cmd = 'G')) do
    begin
    case headcmd^.cmd of
        '+': ; // do nothing - it is just an empty packet
        '#': begin
            AddFileLog('hiTicks increment by remote message');
            inc(hiTicks);
            end;
        'L': ParseCommand('+left', true);
        'l': ParseCommand('-left', true);
        'R': ParseCommand('+right', true);
        'r': ParseCommand('-right', true);
        'U': ParseCommand('+up', true);
        'u': ParseCommand('-up', true);
        'D': ParseCommand('+down', true);
        'd': ParseCommand('-down', true);
        'Z': ParseCommand('+precise', true);
        'z': ParseCommand('-precise', true);
        'A': ParseCommand('+attack', true);
        'a': ParseCommand('-attack', true);
        'S': ParseCommand('switch', true);
        'j': ParseCommand('ljump', true);
        'J': ParseCommand('hjump', true);
        ',': ParseCommand('skip', true);
        'c': begin
            s:= copy(headcmd^.str, 2, Pred(headcmd^.len));
            ParseCommand('gencmd ' + s, true);
             end;
        's': ParseChatCommand('chatmsg ', headcmd^.str, 2);
        'b': ParseChatCommand('chatmsg ' + #4, headcmd^.str, 2);
        'F': ParseCommand('teamgone u' + copy(headcmd^.str, 2, Pred(headcmd^.len)), true);
        'G': ParseCommand('teamback u' + copy(headcmd^.str, 2, Pred(headcmd^.len)), true);
        'f': ParseCommand('teamgone s' + copy(headcmd^.str, 2, Pred(headcmd^.len)), true);
        'g': ParseCommand('teamback s' + copy(headcmd^.str, 2, Pred(headcmd^.len)), true);
        'N': begin
            tmpflag:= false;
            lastTurnChecksum:= SDLNet_Read32(@headcmd^.str[2]);
            AddFileLog('got cmd "N": time '+IntToStr(hiTicks shl 16 + headcmd^.loTime))
             end;
        'p': begin
            x32:= SDLNet_Read32(@(headcmd^.str[2]));
            y32:= SDLNet_Read32(@(headcmd^.str[6]));
            doPut(x32, y32, false)
             end;
        'P': begin
            // these are equations solved for CursorPoint
            // SDLNet_Read16(@(headcmd^.X)) == CursorPoint.X - WorldDx;
            // SDLNet_Read16(@(headcmd^.Y)) == cScreenHeight - CursorPoint.Y - WorldDy;
            if CurrentTeam^.ExtDriven then
               begin
               TargetCursorPoint.X:= LongInt(SDLNet_Read32(@(headcmd^.str[2]))) + WorldDx;
               TargetCursorPoint.Y:= cScreenHeight - LongInt(SDLNet_Read32(@(headcmd^.str[6]))) - WorldDy;
               if not bShowAmmoMenu and autoCameraOn then
                    CursorPoint:= TargetCursorPoint
               end
             end;
        'w': ParseCommand('setweap ' + headcmd^.str[2], true);
        't': ParseCommand('taunt ' + headcmd^.str[2], true);
        'h': ParseCommand('hogsay ' + copy(headcmd^.str, 2, Pred(headcmd^.len)), true);
        '1'..'5': ParseCommand('timer ' + headcmd^.cmd, true);
        else
            if (byte(headcmd^.cmd) >= 128) and (byte(headcmd^.cmd) <= 128 + cMaxSlotIndex) then
                ParseCommand('slot ' + char(byte(headcmd^.cmd) - 79), true)
                else
                OutError('Unexpected protocol command: ' + headcmd^.cmd, True)
        end;
    RemoveCmd
    end;

if (headcmd <> nil) and tmpflag and (not CurrentTeam^.hasGone) then
    TryDo(GameTicks < LongWord(hiTicks shl 16) + headcmd^.loTime,
            'oops, queue error. in buffer: ' + headcmd^.cmd +
            ' (' + IntToStr(GameTicks) + ' > ' +
            IntToStr(hiTicks shl 16 + headcmd^.loTime) + ')',
            true);

isInLag:= (headcmd = nil) and tmpflag and (not CurrentTeam^.hasGone);

if isInLag and fastUntilLag then 
begin
    ParseCommand('spectate 0', true);
    fastUntilLag:= false
end;
end;

procedure chFatalError(var s: shortstring);
begin
    SendIPC('E' + s);
    // TODO: should we try to clean more stuff here?
    SDL_Quit;

    if IPCSock <> nil then
        halt(HaltFatalError)
    else
        halt(HaltFatalErrorNoIPC);
end;

procedure doPut(putX, putY: LongInt; fromAI: boolean);
begin
if CheckNoTeamOrHH or isPaused then
    exit;
bShowFinger:= false;
if (not CurrentTeam^.ExtDriven) and bShowAmmoMenu then
    begin
    bSelected:= true;
    exit
    end;

with CurrentHedgehog^.Gear^,
    CurrentHedgehog^ do
    if (State and gstChooseTarget) <> 0 then
        begin
        isCursorVisible:= false;
        if not CurrentTeam^.ExtDriven then
            begin
            if fromAI then
                begin
                TargetPoint.X:= putX;
                TargetPoint.Y:= putY
                end
            else
                begin
                TargetPoint.X:= CursorPoint.X - WorldDx;
                TargetPoint.Y:= cScreenHeight - CursorPoint.Y - WorldDy;
                end;
            SendIPCXY('p', TargetPoint.X, TargetPoint.Y);
            end
        else
            begin
            TargetPoint.X:= putX;
            TargetPoint.Y:= putY
            end;
        AddFileLog('put: ' + inttostr(TargetPoint.X) + ', ' + inttostr(TargetPoint.Y));
        State:= State and (not gstChooseTarget);
        if (Ammoz[CurAmmoType].Ammo.Propz and ammoprop_AttackingPut) <> 0 then
            Message:= Message or (gmAttack and InputMask);
        end
    else
        if CurrentTeam^.ExtDriven then
            OutError('got /put while not being in choose target mode', false)
end;

procedure initModule;
begin
    RegisterVariable('fatal', @chFatalError, true );

    IPCSock:= nil;

    headcmd:= nil;
    lastcmd:= nil;
    isPonged:= false;
    SocketString:= '';

    hiTicks:= 0;
    flushDelayTicks:= 0;
    sendBuffer.count:= 0;
end;

procedure freeModule;
begin
    while headcmd <> nil do RemoveCmd;
    SDLNet_FreeSocketSet(fds);
    SDLNet_TCP_Close(IPCSock);
    SDLNet_Quit();

end;

end.
